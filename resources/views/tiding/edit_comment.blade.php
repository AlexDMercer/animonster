@extends('app')

@section('title')
	AniMonster
@endsection

@section('content')
	<div class="card mb-3">
	  <img src="{{$tiding->img}}" class="card-img-top" alt="...">
	  <div class="card-body">
	    <h5 class="card-title">{{$tiding->title}}</h5>
	    <p class="card-text">{{$tiding->body}}</p>
	    <p class="card-text"><small class="text-muted">{{$tiding->created_at}}</small></p>
	  </div>
	</div>
	<br><hr><br>

	@include('inc.errors')
	<div class="container">
		<form method="POST" action="{{route('t_update', [$tiding->id, $comment->id])}}" class="card col-12">
			@csrf
			<div class="card-block form-group mt-3">
				<input type="text" name="text" value="{{$comment->text}}" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-dark float-right">Редактировать комментарий</button>
			</div>
		</form>
	</div>
	<br><hr><br>
	<div class="container">
		@foreach($tiding->comments as $comment)
			<div class="card mb-3">
			  <div class="card-header">
		  		@if(isset($comment->user->name))
			    	{{$comment->user->name}}
				    @if($comment->user->id == auth()->id())
					    <a href="{{route('delete', $comment->id)}}" class="btn btn-danger float-right">&cross;</a>
					    <a href="{{route('edit', [$tiding->id, $comment->id])}}" class="btn btn-light float-right">
				    		<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  		<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
							</svg>
						</a>
				    @endif
			    @else
		    		Пользователь удален
	    		@endif
			  </div>
			  <div class="card-body">
			    <blockquote class="blockquote mb-0">
			      <p>{{$comment->text}}</p>
			      <footer class="blockquote-footer">{{$comment->created_at->diffForHumans()}}
			      		<a href="
			      			@if($comment->liked()->find(auth()->id()))
			      				{{route('dislike', [$tiding->id, $comment->id])}}
		      				@else
		      					{{route('like', [$tiding->id, $comment->id])}}
	      					@endif
			      			" class="float-right">{{$comment->like_comment}} &hearts;
			      		</a>
			      </footer>
			    </blockquote>
			  </div>
			</div>
		@endforeach
	</div>
@endsection