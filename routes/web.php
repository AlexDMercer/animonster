<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('about', function () { return view('about'); })->name('about');
//Route::get('admin', function () { return view('admin.index'); })->name('admin');
Route::get('area', function () { return view('area'); })->name('area');
Route::get('admin', 'StatisticController@index')->name('admin');
Route::get('admin/films', 'StatisticController@films')->name('view_films');
Route::get('admin/films/sort', 'StatisticController@sortFilms')->name('sort_films');
Route::get('admin/tidings', 'StatisticController@tidings')->name('view_tidings');
Route::get('admin/tidings/sort', 'StatisticController@sortTidings')->name('sort_tidings');

Route::get('/', 'FilmController@viewNew')->name('home');
Route::get('films', 'FilmController@viewFilms')->name('films');
Route::get('serials', 'FilmController@viewSerials')->name('serials');
Route::get('search', 'FilmController@search')->name('search');
Route::get('show/{film}', 'FilmController@viewShow')->name('show');
Route::get('rating', 'FilmController@rating')->name('rating');
Route::group(['prefix'=>'admin'], function()
	{
		Route::get('create', 'FilmController@create')->name('film_create');
		Route::post('create', 'FilmController@store')->name('film_store');
		Route::get('edit/{film}', 'FilmController@edit')->name('film_edit');
		Route::post('edit/{film}', 'FilmController@update')->name('film_update');
		Route::get('{film}/delete', 'FilmController@delete')->name('film_delete');
		Route::resource('users', 'UserController');
	});

Route::resource('tidings', 'TidingController');
Route::resource('notes', 'NoteController');

//Комментарии для фильмов
Route::post('show/{film}/comments', 'CommentController@store')->middleware('auth')->name('comments');
Route::get('show/{film}/comments/edit/{comment}', 'CommentController@edit')->name('edit');
Route::post('show/{film}/comments/edit/{comment}', 'CommentController@update')->name('update');
Route::get('show/{film}/delete', 'CommentController@delete')->name('delete');

//Комментарии для новостей
Route::post('tidings/{tiding}/comments', 'CommentController@t_store')->middleware('auth')->name('t_comments');
Route::get('tidings/{tiding}/comments/edit/{comment}', 'CommentController@t_edit')->name('t_edit');
Route::post('tidings/{tiding}/comments/edit/{comment}', 'CommentController@t_update')->name('t_update');
Route::get('tidings/{tiding}/delete', 'CommentController@t_delete')->name('t_delete');

//Лайки для комментариев
Route::get('show/{film}/comments/like', 'CommentController@like')->middleware('auth')->name('like');
Route::get('show/{film}/comments/dislike', 'CommentController@dislike')->middleware('auth')->name('dislike');

/*Route::get('show/{film}/comments/{comment}/like', 'CommentController@like')->middleware('auth')->name('like');
Route::get('show/{film}/comments/{comment}/dislike', 'CommentController@dislike')->middleware('auth')->name('dislike');*/

Route::get('ajax', function () { return view('ajax'); })->name('ajax');
Route::post('ajax', 'CommentController@ajax')->middleware('auth')->name('cajax');
