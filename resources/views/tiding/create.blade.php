@extends('admin.app')

@section('title')
	Добавить новость
@endsection

@section('content')
	<h2>Добавить фильм</h2>
	<div class="card">
		<div class="card-body">
			<form action="{{route('tidings.store')}}" method="POST">
				@csrf
				<div class="form-group">
				    <label for="img">Постер</label>
				    <input type="text" name="img" class="form-control" id="img" placeholder="URL постера">
			  	</div>
				<div class="form-group">
				    <label for="title">Заголовок</label>
				    <input type="text" name="title" class="form-control" id="title">
				</div>
				<div class="form-group">
				    <label for="body">Текст новости</label>
				    <textarea type="text" name="body" class="form-control" id="body"></textarea>
				</div>
			  	<button type="submit" class="btn btn-dark float-right">Добавить</button>
			</form>
		</div>
	</div>
@endsection