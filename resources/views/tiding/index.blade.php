@extends('app')

@section('title')
	AniMonster
@endsection

@section('content')
	<h2>Новости</h2><hr>
	@foreach($tidings as $tiding)
		<div class="card mb-3">
		  <img src="{{$tiding->img}}" class="card-img-top" alt="...">
		  <div class="card-body">
		    <h5 class="card-title" style="color: #fffff3">{{$tiding->title}}</h5>
		    <p class="card-text"><small class="text-muted align-bottom">{{$tiding->created_at}}</small>
	    	<a href="{{route('tidings.show', $tiding->id)}}" class="btn btn-dark float-right">Подробнее</a></p>
		  </div>
		</div>
	@endforeach
	<div class="float-right">{{$tidings->render()}}</div>
@endsection