@extends('app')

@section('title')
	Фильмы
@endsection

@section('content')
	<h2>Личный кабинет</h2><hr>
	<div class="container">
  		<table class="table table-striped table-dark">
		  <thead>
		    <tr>
		      <th scope="col" class="text-center">Аниме</th>
		      <th scope="col" class="text-center">Сезон</th>
		      <th scope="col" class="text-center">Серия</th>
		      <th scope="col" class="text-center">Действие</th>
		    </tr>
		  </thead>
		  <tbody>
		  		@foreach($user->notes as $note)
				    <tr>
				      	<td class="text-center"><a href="{{route('show', $note)}}">{{$note->name}}</a></td>
				      	<td class="text-center">{{$note->pivot->season}}</td>
				      	<td class="text-center">{{$note->pivot->series}}</td>
				      	<td class="text-center"> 
			      			<a href="{{route('notes.edit', $note)}}" class="btn btn-dark">
				    			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  			<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
								</svg>
							</a>
				      		<form action="{{route('notes.destroy', $note->id)}}" method="POST" class="d-inline">
				    			{{ method_field('DELETE') }}
				    			@csrf
				    			<button type="submit" class="btn btn-danger">&cross;</button>
				    		</form>

				      	</td>
				    </tr>
			    @endforeach
		  </tbody>
		</table>
		<p><a href="{{route('notes.create')}}" class="btn btn-dark float-right" type="submit">Добавить запись</a></p>
	</div>	
@endsection