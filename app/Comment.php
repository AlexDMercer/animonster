<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Film;
use App\User;
use App\Tiding;

class Comment extends Model
{
	protected $fillable = ['text', 'commentable_id', 'commentable_type', 'user_id'];

    public function commentable()
    {
    	return $this->morphTo();
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

	public function liked()
    {
    	return $this->belongsToMany(User::class);
    }    
}
