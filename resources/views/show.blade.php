@extends('app')

@section('title')
	AniMonster
@endsection

@section('content')
	<div class="container">
		<div class="card show">
			<div id="card-header">
		  		<iframe width="100%" height="100%" src="{{$film->frame}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
		  		</iframe>
		  	</div>
		  <div class="card-body">
		    <h2 class="card-title text-center">{{$film->name}}
	    	@if(auth()->user()!==null)
		    	@if(auth()->user()->role == 'admin')
				    <a href="{{route('film_delete', $film->id)}}" class="btn btn-danger float-right">&cross;</a>
				    <a href="{{route('film_edit', $film->id)}}" class="btn btn-dark float-right">
			    		<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  		<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
						</svg>
					</a>
		    	@endif
	    	@endif
		    </h2>
		    <p class="card-text">{{$film->description}}</p>
		    <ul class="list-group list-group-horizontal float-right">
			  <li class="list-group-item">Автор
			  	<span class="badge badge-dark badge-pill">{{$film->author}}</span>
			  </li>
			  <li class="list-group-item">Год выпуска
			  	<span class="badge badge-dark badge-pill">{{$film->year}}</span>
			  </li>
			  <li class="list-group-item">Рейтинг
			  	<span class="badge badge-dark badge-pill">{{$film->rating}}</span>
			  </li>
			  <li class="list-group-item">Просмотров
			  	<span class="badge badge-dark badge-pill">{{$film->view_count}}</span>
			  </li>
			</ul>
		  </div>
		</div>
	</div>
	<br><hr><h2>Комментарии</h2><br>
	<div class="container">
		@foreach($film->comments as $comment)
			<div class="card mb-3">
			  <div class="card-header">
		  		@if(isset($comment->user->name))
			    	{{$comment->user->name}}
			    	@if(auth()->user()!==null)
					    @if($comment->user->id == auth()->user()->id || auth()->user()->role == 'admin')
						    <a href="{{route('delete', $comment->id)}}" class="btn btn-danger float-right">&cross;</a>
					    @endif
					    @if($comment->user->id == auth()->id())
						    <a href="{{route('edit', [$film->id, $comment->id])}}" class="btn btn-light float-right">
					    		<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  		<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
								</svg>
							</a>
					    @endif
				    @endif
			    @else
		    		- Пользователь удален -
		    		@if(auth()->user()!==null)
			    		@if(auth()->user()->role == 'admin')
						    <a href="{{route('delete', $comment->id)}}" class="btn btn-danger float-right">&cross;</a>
					    @endif
				    @endif
	    		@endif
			  </div>
			  <div class="card-body">
			    <blockquote class="blockquote mb-0">
			      <p>{{$comment->text}}</p>
			      <footer class="blockquote-footer">{{$comment->created_at->diffForHumans()}}
			      		<span id="submit" class="submit float-right" style="cursor: pointer" data-order="{{$comment->id}}" value="{{$comment->like_comment}}">
			      			<div id="comment_{{$comment->id}}" style="" class=" 
			      				@if($comment->liked()->find(auth()->id()))
			      					like
			      				@endif
			      			">
			      				{{$comment->like_comment}} &hearts;
			      			</div>
			      		</span>
			      </footer>
			    </blockquote>
			  </div>
			</div>		
		@endforeach
	</div>

	<br><hr><br>

	<div class="container">
		<form method="POST" action="{{route('comments', $film->id)}}" class="card col-12">
			@csrf
			<div class="card-block form-group mt-3">
				<input type="text" name="text" placeholder="Ваш комментарий.." class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-dark float-right">Добавить комментарий</button>
			</div>
		</form>
	</div>

	<script>
		function funcSuccess(data) {
			data = JSON.parse(data);
			$("#comment_" + data.comment_id).html(data.likes + " &hearts;");
			if($("#comment_" + data.comment_id).hasClass("like")) {
				$("#comment_" + data.comment_id).removeClass("like");
			} else {
				$("#comment_" + data.comment_id).addClass("like");
			}
		}

		$(document).ready(function() {
			$(".submit").click(function() {
				var film = {{$film->id}};
				var comment = $(this).data('order');
				$.ajax({
					url: '{{route('like', $film->id)}}',
					type: 'GET',
					data: {film: film, comment: comment},
					dataType: "html",
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
					success: funcSuccess
				});
			});
		});
	</script>

@endsection


			