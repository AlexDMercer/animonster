<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\User;
use App\Tiding;

class TidingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tidings = Tiding::latest()->paginate(3);
        return view('tiding.index', compact('tidings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiding.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tiding = Tiding::create([
            'title' => $request->title,
            'body' => $request->body,
            'img' => $request->img
        ]);

        return redirect()->route('tidings.show', compact('tiding'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tiding $tiding)
    {
        event('tidingHasViewed', $tiding);
        return view('tiding.show', compact('tiding'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiding $tiding)
    {
        return view('tiding.edit', compact('tiding'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Tiding $tiding, Request $request)
    {
        $up_tiding = Tiding::find($tiding->id)->update([
            'title' => $request->title,
            'body' => $request->body,
            'img' => $request->img
        ]);

        return redirect()->route('tidings.show', compact('tiding'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tiding = Tiding::find($id)->delete();
        return redirect()->route('tidings.index');
    }
}
