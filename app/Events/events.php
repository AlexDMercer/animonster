<?php

Event::listen('show', function($film) {
	$film->increment('view_count');
});