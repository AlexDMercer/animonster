<div class="card card-info">
	<div class="card-header">
		<div class="sidebar-header text-center">Административная панель</div>
	</div>
	<div class="card-body">
		<p>Статистика &dtrif;</p>
	  	<div class="">
	   		<p><a href="{{route('admin')}}">- Посетители</a></p>
	   		<p><a href="{{route('view_films')}}">- Фильмы</a></p>
	   		<p><a href="{{route('view_tidings')}}">- Новости</a></p>
	  	</div>
		
		<p><a href="{{route('users.index')}}">Пользователи</a></p>
		<p><a href="{{route('film_create')}}">Добавить фильм</a></p>
		<p><a href="{{route('tidings.create')}}">Добавить новость</a></p>
		<p><a href="{{route('notes.index')}}">Тетрадь аниме</a></p>
	</div>
</div>
<br>