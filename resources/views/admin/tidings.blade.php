@extends('admin.app')

@section('title')
	Adminka
@endsection

@section('content')
	<div class="container">
		<h2>Статистика просмотра новостей </h2><hr>
	  	<table class="table table-striped table-dark">
		  <thead>
		    <tr>
		      <th scope="col" class="text-center">Новость</th>
		      <th scope="col" class="text-center"><a href="{{route('view_tidings')}}">Просмотров</a></th>
		      <th scope="col" class="text-center"><a href="{{route('sort_tidings')}}">Комментариев</a></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($tidings as $tiding)
			    <tr>
			      <td class="text-center"><a href="{{route('tidings.show', $tiding->id)}}">{{$tiding->title}}</a></td>
			      <td class="text-center">{{$tiding->view_count}}</td>
			      <td class="text-center">{{$tiding->comments->count()}}</td>
			    </tr>
			@endforeach
		  </tbody>
		</table>	
		<div class="float-right">{{$tidings->render()}}</div>  	
	</div>
@endsection