<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'user_id' => 
			'name' => $faker->sentence,
			'description' => $faker->paragraph,
			'author' => $faker->name,
        ]);
    }
}
