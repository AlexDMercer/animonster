@extends('admin.app')

@section('title')
	Пользователи
@endsection

@section('content')
	<h2>Пользователи</h2><hr>
	<table class="table table-striped table-dark">
	  	<thead>
		    <tr>
		      <th scope="col" class="text-center"><a href="{{route('users.create', $user->id)}}">Пользователь</a></th>
		      <th scope="col" class="text-center">Email</th>
		      <th scope="col" class="text-center">Роль</th>
		      <th scope="col" class="text-center">Последняя активность</th>
		      <th scope="col" class="text-center">Редактировать</th>
		      <th scope="col" class="text-center">Удалить</th>
		    </tr>
	 	</thead>
	  	<tbody>
	  		<form action="{{route('users.update', $user->id)}}" method="POST">
	  			{{ method_field('PUT') }}
				@csrf 
			    <tr>
			      	<td class="text-center">
			      		<input type="text" name="name" value="{{$user->name}}" style="width: 100%"> 
			      	</td>
			      	<td class="text-center">
			      		<input type="text" name="email" value="{{$user->email}}" style="width: 100%">
			      	</td>
			      	<td class="text-center">
			      		<select name="role">
						  	<option value="admin" @if($user->role == 'admin') selected @endif>Admin</option>
						  	<option value="guest" @if($user->role == 'guest') selected @endif>Guest</option>
						</select>
			      	</td>
			      	<td class="text-center">??</td>
			      	<td class="text-center">
			      		<button type="submit" class="btn btn-dark">
			    			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check2-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M15.354 2.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L8 9.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
							  <path fill-rule="evenodd" d="M1.5 13A1.5 1.5 0 0 0 3 14.5h10a1.5 1.5 0 0 0 1.5-1.5V8a.5.5 0 0 0-1 0v5a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V3a.5.5 0 0 1 .5-.5h8a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 3v10z"/>
							</svg>
						</button>
					</td>
			      	<form action="{{route('users.destroy', $user->id)}}" method="POST">
						{{ method_field('DELETE') }}
						@csrf 
				      	<td class="text-center">
				      		<button type="submit" class="btn btn-danger">&cross;</button>
				      	</td>
		      		</form>
			    </tr>
			</form>
		@foreach($users as $user)
		    <tr>
		      	<td class="text-center">{{$user->name}}</td>
		      	<td class="text-center">{{$user->email}}</td>
		      	<td class="text-center">{{$user->role}}</td>
		      	<td class="text-center">??</td>
		      	<td class="text-center">
		      		<a href="{{route('users.edit', $user->id)}}" class="btn btn-dark">
		    			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				  			<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
						</svg>
					</a>
				</td>
		      	<form action="{{route('users.destroy', $user->id)}}" method="POST">
					{{ method_field('DELETE') }}
					@csrf 
			      	<td class="text-center">
			      		<button type="submit" class="btn btn-danger">&cross;</button>
			      	</td>
		      	</form>
		    </tr>
		@endforeach
	  </tbody>
	</table>
@endsection