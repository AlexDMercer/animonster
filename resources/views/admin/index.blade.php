@extends('admin.app')

@section('title')
	Adminka
@endsection

@section('content')
	<div class="container">
		<h2>Кабинет администратора</h2><hr>
  		<table class="table table-striped table-dark">
		  <thead>
		    <tr>
		      <th scope="col" class="text-center">Дата</th>
		      <th scope="col" class="text-center">Посетителей</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($statistics as $statistic)
			    <tr>
			      <td class="text-center">{{date("d.m.Y", strtotime($statistic->created_at))}}</td>
			      <td class="text-center">{{$statistic->visit}}</td>
			    </tr>
			@endforeach
		  </tbody>
		</table>
	</div>	  	
@endsection