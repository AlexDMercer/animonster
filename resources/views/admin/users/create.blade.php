@extends('admin.app')

@section('title')
	Пользователи
@endsection

@section('content')
	<h2>Пользователи</h2><hr>
	<table class="table table-striped table-dark">
	  	<thead>
		    <tr>
		      <th scope="col" class="text-center"><a href="{{route('users.create')}}">Пользователь</a></th>
		      <th scope="col" class="text-center">Email</th>
		      <th scope="col" class="text-center">Роль</th>
		      <th scope="col" class="text-center">Последняя активность</th>
		      <th scope="col" class="text-center">Редактировать</th>
		      <th scope="col" class="text-center">Удалить</th>
		    </tr>
	 	</thead>
	  	<tbody>
	  		<form action="{{route('users.store')}}" method="POST">
				@csrf 
			    <tr>
			      	<td class="text-center">
			      		<input type="text" name="name" style="width: 100%"> 
			      	</td>
			      	<td class="text-center">
			      		<input type="email" name="email" style="width: 100%">
			      	</td>
			      	<td class="text-center">
			      		<select name="role">
						  	<option value="admin">Admin</option>
						  	<option value="guest" selected>Guest</option>
						</select>
			      	</td>
			      	<td class="text-center">
			      		<input type="password" name="password" placeholder="пароль" style="width: 100%">
			      	</td>
			      	<td class="text-center">
			      		<button type="submit" class="btn btn-dark">
			    			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check2-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M15.354 2.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L8 9.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
							  <path fill-rule="evenodd" d="M1.5 13A1.5 1.5 0 0 0 3 14.5h10a1.5 1.5 0 0 0 1.5-1.5V8a.5.5 0 0 0-1 0v5a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V3a.5.5 0 0 1 .5-.5h8a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 3v10z"/>
							</svg>
						</button>
					</td>
					<td class="text-center">
						<a class="btn btn-dark">
							<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-emoji-sunglasses" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
							  <path fill-rule="evenodd" d="M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM6.5 6.497V6.5h-1c0-.568.447-.947.862-1.154C6.807 5.123 7.387 5 8 5s1.193.123 1.638.346c.415.207.862.586.862 1.154h-1v-.003l-.003-.01a.213.213 0 0 0-.036-.053.86.86 0 0 0-.27-.194C8.91 6.1 8.49 6 8 6c-.491 0-.912.1-1.19.24a.86.86 0 0 0-.271.194.213.213 0 0 0-.036.054l-.003.01z"/>
							  <path d="M2.31 5.243A1 1 0 0 1 3.28 4H6a1 1 0 0 1 1 1v1a2 2 0 0 1-2 2h-.438a2 2 0 0 1-1.94-1.515L2.31 5.243zM9 5a1 1 0 0 1 1-1h2.72a1 1 0 0 1 .97 1.243l-.311 1.242A2 2 0 0 1 11.439 8H11a2 2 0 0 1-2-2V5z"/>
							</svg>
						</a>
					</td>
			    </tr>
			</form>
		@foreach($users as $user)
		    <tr>
		      	<td class="text-center">{{$user->name}}</td>
		      	<td class="text-center">{{$user->email}}</td>
		      	<td class="text-center">{{$user->role}}</td>
		      	<td class="text-center">??</td>
		      	<td class="text-center">
		      		<a href="{{route('users.edit', $user->id)}}" class="btn btn-dark">
		    			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				  			<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
						</svg>
					</a>
				</td>
		      	<form action="{{route('users.destroy', $user->id)}}" method="POST">
					{{ method_field('DELETE') }}
					@csrf 
			      	<td class="text-center">
			      		<button type="submit" class="btn btn-danger">&cross;</button>
			      	</td>
		      	</form>
		    </tr>
		@endforeach
	  </tbody>
	</table>
@endsection