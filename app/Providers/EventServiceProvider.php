<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Film;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('filmHasViewed', function ($film) {
            session_start();
            $id = $film->id;

            /*if (Session::get('lol') != $id) {
                $film->increment('view_count');
                Session::push('lol', $id);
            }*/

            if (!isset($_SESSION['view_film'])) {
                $_SESSION['view_film'] = [];
            }

            if (!in_array($id, $_SESSION['view_film'])) {
                $film->increment('view_count');
                $_SESSION['view_film'][] = $id;
            }

        });

        Event::listen('tidingHasViewed', function ($tiding) {
            session_start();
            $id = $tiding->id;

            if (!isset($_SESSION['view_tiding'])) {
                $_SESSION['view_tiding'] = [];
            }

            if (!in_array($id, $_SESSION['view_tiding'])) {
                $tiding->increment('view_count');
                $_SESSION['view_tiding'][] = $id;
            }

        });
    }
}
