<nav class="navbar navbar-expand-lg navbar-dark bg-dark navbg">
  <img src="/img/2к.jpg" width="35" height="38" id="header">
  <a href="/" class="link"><h1>AniMonster</h1></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="/">Новинки <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('films')}}">Фильмы</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('serials')}}" tabindex="-1" aria-disabled="true">Сериалы</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('rating')}}" tabindex="-1" aria-disabled="true">Рейтинг</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('tidings.index')}}" tabindex="-1" aria-disabled="true">Новости</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('about')}}" tabindex="-1" aria-disabled="true">О нас</a>
      </li>
    </ul>
  </div>


  @if (Route::has('login'))
    @auth
      <div>
        <a href="
          @if(Auth::user()->role == 'admin')
            {{route('admin')}}
          @else
            {{ route('notes.index') }}
          @endif
        ">{{Auth::user()->name}} </a> |
        <a href="{{ route('logout') }}" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"> {{ __('Выход') }}</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
      </div>
  @else
      <div>
        <a href="{{ route('login') }}">Войти </a> |
        <a href="{{ route('register') }}"> Регистрация</a>
      </div>
    @endauth
  @endif



</nav>
<div class="container">
  <form action="" role="search" class="search d-lg-none d-xl-none">
    <div class="input-group mb-3">
      <input type="search" class="form-control" placeholder="найти.." aria-label="Recipient's username" aria-describedby="button-addon2">
      <div class="input-group-append">
        <button class="btn btn-outline-secondary" type="button" id="button-addon2">
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
          </svg>
        </button>
      </div>
    </div>
  </form>
</div>