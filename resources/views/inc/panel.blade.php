<div class="card card-info d-none d-lg-block d-xl-block " id="search">
	<div class="card-header">
		<div class="sidebar-header">Поиск</div>
	</div>
	<div class="card-body">
		<form action="{{route('search')}}" role="search" method="get">
		    <div class="input-group mb-3">
		      <input type="search" class="form-control" name="search" placeholder="найти.." aria-label="Recipient's username" aria-describedby="button-addon2">
		      <div class="input-group-append">
		        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">
		          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
		            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
		            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
		          </svg>
		        </button>
		      </div>
		    </div>
		</form>
    </div>
</div>
<br>
<div class="card card-info">
	<div class="card-header">
		<div class="sidebar-header"><a href="{{route('tidings.index')}}" class="rating">Последние новости</a></div>
	</div>
	<div class="card-body">
		@foreach($last_tidings as $tiding)
			<p><a href="{{route('tidings.show', $tiding->id)}}">{{$tiding->title}}</a></p>
		@endforeach
	</div>
</div>
<br>
<div class="card card-info">
	<div class="card-header">
		<div class="sidebar-header"><a href="{{route('rating')}}" class="rating">Топ 5 аниме</a></div>
	</div>
	<div class="card-body">
		<ul class="list-group">
			@foreach($films_rating as $film)
		        <li class="list-group-item list-group-warning">
		          <a href="{{route('show', $film->id)}}">{{$film->name}}</a>
		          <span class="badge float-right">{{$film->rating}}</span>
		        </li>
	        @endforeach
    	</ul>
	</div>
</div>