<div class="container">
	<div class="row">
		@foreach($film->comments as $comment)
			<div class="card col-12">
			  <div class="card-header">
			    User_name
			  </div>
			  <div class="card-body">
			    <blockquote class="blockquote mb-0">
			      <p>{{$comment->text}}</p>
			      <footer class="blockquote-footer">{{$comment->created_at}}</footer>
			    </blockquote>
			  </div>
			</div>
		@endforeach
	</div>
</div>