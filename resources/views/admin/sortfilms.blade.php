@extends('admin.app')

@section('title')
	Adminka
@endsection

@section('content')
	<div class="container">
		<h2>Статистика просмотра фильмов</h2><hr>
  		<table class="table table-striped table-dark">
		  <thead>
		    <tr>
		      <th scope="col" class="text-center">Аниме</th>
		      <th scope="col" class="text-center"><a href="{{route('view_films')}}">Просмотров</a></th>
		      <th scope="col" class="text-center"><a href="{{route('sort_films')}}">Комментариев</a></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($films as $film)
			    <tr>
			      <td class="text-center"><a href="{{route('show', $film->id)}}">{{$film->name}}</a></td>
			      <td class="text-center">{{$film->view_count}}</td>
			      <td class="text-center">{{$film->comments->count()}}</td>
			    </tr>
			@endforeach
		  </tbody>
		</table> 
		<div class="float-right">{{$films->render()}}</div>	
	</div>
@endsection