<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'film_user';

    protected $fillable = ['film_id', 'season', 'series'];
}
