@extends('app')

@section('title')
	Фильмы
@endsection

@section('content')
	<h2>Личный кабинет</h2><hr>
	<div class="container">
  		<table class="table table-striped table-dark">
		  <thead>
		    <tr>
		      <th scope="col" class="text-center">Аниме</th>
		      <th scope="col" class="text-center">Сезон</th>
		      <th scope="col" class="text-center">Серия</th>
		      <th scope="col" class="text-center">Добавить</th>
		    </tr>
		  </thead>
		  <tbody>
  			<form action="{{route('notes.store')}}" method="POST">
				@csrf
			    <tr>
			      	<td class="text-center">
			      		<div class="mb-3">
						    <select class="custom-select" name="name" required>
						      	<option value="">Choose...</option>
						      	@foreach($films as $film)
						      		<option value="{{$film->id}}">{{$film->name}}</option>
					      		@endforeach
						    </select>
						</div>
			      	</td>
			      	<td class="text-center">
			      		<input type="text" name="season">
			      	</td>
			      	<td class="text-center">
			      		<input type="text" name="series">
			      	</td>
			      	<td class="text-center">
			      		<button class="btn btn-dark" type="submit">
			      			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check2-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  		<path fill-rule="evenodd" d="M15.354 2.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L8 9.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
						  		<path fill-rule="evenodd" d="M1.5 13A1.5 1.5 0 0 0 3 14.5h10a1.5 1.5 0 0 0 1.5-1.5V8a.5.5 0 0 0-1 0v5a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V3a.5.5 0 0 1 .5-.5h8a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 3v10z"/>
							</svg>
			      		</button>
		      		</td>
			    </tr>
    		</form>
			    @foreach($user->notes as $note)
				    <tr>
				      	<td class="text-center"><a href="{{route('show', $note->id)}}">{{$note->name}}</a></td>
				      	<td class="text-center">{{$note->pivot->season}}</td>
				      	<td class="text-center">{{$note->pivot->series}}</td>
				      	<td class="text-center">
				      		<a href="{{route('notes.edit', $note->id)}}" class="btn btn-dark">
			    				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  				<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
								</svg>
						</a>
			      		<form action="{{route('notes.destroy', $note->id)}}" method="POST" class="d-inline">
			    			{{ method_field('DELETE') }}
			    			@csrf
			    			<button type="submit" class="btn btn-danger">&cross;</button>
			    		</form>
				      	</td>
			    	</tr>
		   		 @endforeach
		  </tbody>
		</table>			
	</div>	
@endsection