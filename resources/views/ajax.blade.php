<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/style.css">
	<script src="{{ asset('js/app.js') }}" defer></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<meta name="csrf-token" content="{{csrf_token()}}">
	<title>@yield('title')</title>
</head>
<body>
	<p id="load" style="cursor: pointer; color: black;">Загрузка данных</p>
	<div id="info" style="color: black"></div>

	<script>

		function funcBefore() {
			console.log('before');
			$("#info").text("Ожидание данных...");
		}

		function funcSuccess(data) {
			console.log('hello');
			$("#info").text(data);
		}

		$(document).ready(function() {
			$("#load").bind("click", function() {
				var num = "1";
				console.log(num);
				$.ajax({
					url: "{{route('cajax')}}",
					type: 'POST',
					data: {num: num},
					dataType: "html",
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
					beforeSend: funcBefore,
					success: funcSuccess
				});
			});
		});

	</script>
</body>
</html>

<a href="
	@if($comment->liked()->find(auth()->id()))
		{{route('dislike', [$film->id, $comment->id])}}
	@else
		{{route('like', [$film->id, $comment->id])}}
	@endif
	" class="float-right">{{$comment->like_comment}} &hearts;
</a>