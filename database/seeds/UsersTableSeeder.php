<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Comment;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   /* public function run()
    {
        //DB::table('users')->insert([
        User::create([
        	'name' => str_random(10),
			'email' => str_random(10).'@gmail.com',
			'password' => bcrypt('secret'),
			'remember_token' => srt_random(10),
        ]);
    }*/

    public function run() 
    {
    	factory(User::class, 5)->create()->each(function($u) {
    		$u->comment()->save(factory(Comment::class)->make());
    	});
    }
}
