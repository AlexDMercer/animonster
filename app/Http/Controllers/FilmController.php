<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Film;
use App\Comment;
use App\User;

class FilmController extends Controller
{
    public function viewNew()
    {
    	/*$films = new Film;
    	return view('home', ['films' => $films->all()]);*/
        $films = Film::where('category', 1)->latest('year')->take(4)->get();
        $serials = Film::where('category', 2)->latest('year')->take(4)->get();
        return view('home', compact('films', 'serials'));
    }

    public function viewFilms()
    {
    	$films = new Film;
    	return view('films', ['films' => $films->where('category', 1)->paginate(8)]);
    }

    public function viewSerials()
    {
    	$films = new Film;
    	return view('serials', ['films' => $films->where('category', 2)->paginate(8)]);
    }

    /*public function viewShow($id)
    {
    	$film = new Film;
    	return view('show', ['film' => $film->find($id)]);
    }*/

    public function viewShow(Film $film, Request $request)
    {
        $user = $request->user();
        event('filmHasViewed', $film);
        return view('show', compact('film', 'user', 'request'));
    }

    public function rating()
    {
        $films = Film::orderBy('rating', 'desc')->get();
        return view('rating', compact('films'));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        $films = Film::where('name', 'LIKE', "%{$search}%")
                            ->orWhere('description', 'LIKE', "%{$search}%")
                            ->orWhere('author', 'LIKE', "%{$search}%")
                            ->orWhere('year', 'LIKE', "%{$search}%")
                            ->paginate(4);
        return view('search', ['films' => $films]);
    }

    public function create()
    {
        return view('admin.create');
    }

    public function store(Request $request)
    {
        $film = Film::create([
            'name' => $request->name,
            'description' => $request->description,
            'category' => $request->category,
            'year' => $request->year,
            'author' => $request->author,
            'rating' => $request->rating,
            'poster' => $request->poster,
            'frame' => $request->frame
        ]);

        return redirect()->route('show', compact('film'));
    }

    public function edit(Film $film)
    {
        return view('admin.edit', compact('film'));
    }

    public function update(Film $film, Request $request)
    {
        $up_film = Film::find($film->id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'category' => $request->category,
            'year' => $request->year,
            'author' => $request->author,
            'rating' => $request->rating,
            'poster' => $request->poster,
            'frame' => $request->frame
        ]);

        return redirect()->route('show', compact('film'));
    }

    public function delete($id)
    {
        $film = Film::find($id)->delete();
        return redirect()->route('admin');
    }
}
