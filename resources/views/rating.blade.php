@extends('app')

@section('title')
	AniMonster
@endsection

@section('content')
	<h2>Рейтинг</h2><hr>
	<table class="table table-striped table-dark">
	  <thead>
	    <tr>
	      <th scope="col" class="text-center">Аниме</th>
	      <th scope="col" class="text-center">Мангака</th>
	      <th scope="col" class="text-center">Год</th>
	      <th scope="col" class="text-center">Рейтинг</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($films as $film)
		    <tr>
		      <td class="text-center"><a href="{{route('show', $film->id)}}">{{$film->name}}</a></td>
		      <td class="text-center">{{$film->author}}</td>
		      <td class="text-center">{{$film->year}}</td>
		      <td class="text-center">{{$film->rating}}</td>
		    </tr>
		@endforeach
	  </tbody>
	</table>
@endsection