<?php 

use App\User;
use App\Film;
use App\Comment;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(User::class, function(Faker\Generator $faker)
{
	return [
		'name' => $faker->name,
		'email' => $faker->safeEmail,
		'password' => bcrypt('secret'),
		'remember_token' => srt_random(10),
	];
});	

$factory->define(Film::class, function (Faker\Generator $faker)
{
	return [
		'user_id' => function() {
			return factory(App\User::class)->create()->id;
		},
		'name' => $faker->sentence,
		'description' => $faker->paragraph,
		'author' => $faker->name,
	];
});

$factory->define(Comment::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'film_id' => function () {
            return factory(App\Post::class)->create()->id;
        },
        'text' => $faker->paragraph
    ];
});