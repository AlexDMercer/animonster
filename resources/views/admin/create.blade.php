@extends('admin.app')

@section('title')
	Добавить фильм
@endsection

@section('content')
	<h2>Добавить фильм</h2><hr>
	<div class="card">
		<div class="card-body">
			<form action="{{route('film_store')}}" method="POST">
				@csrf
			  <div class="form-group">
			    <label for="name">Название</label>
			    <input type="text" name="name" class="form-control" id="name">
			  </div>
			  <div class="form-group">
			    <label for="description">Описание</label>
			    <textarea type="text" name="description" class="form-control" id="description"></textarea>
			  </div>
			  <div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="poster">Постер</label>
			      <input type="text" name="poster" class="form-control" id="poster"  placeholder="URL картинки">
			    </div>
			    <div class="form-group col-md-6">
			      <label for="frame">Фрейм</label>
			      <input type="text" name="frame" class="form-control" id="frame"  placeholder="youtube->поделиться->встроить">
			    </div>
			  </div>
			  <div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="author">Автор</label>
			      <input type="text" name="author" class="form-control" id="author">
			    </div>
			    <div class="form-group col-md-4">
			      <label for="year">Год</label>
			      <input type="text" name="year" class="form-control" id="year">
			    </div>
			    <div class="form-group col-md-2">
			      <label for="rating">Рейтинг</label>
			      <input type="text" name="rating" class="form-control" id="rating">
			    </div>
			  </div>
			  <fieldset class="form-group">
			    <div class="row">
			      <legend class="col-form-label col-sm-2 pt-0"><p>Категория</p></legend>
			      <div class="col-sm-10">
			        <div class="custom-control custom-radio custom-control-inline">
					  <input type="radio" id="film" name="category" value="1" class="custom-control-input">
					  <label class="custom-control-label" for="film">Фильм</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
					  <input type="radio" id="serial" name="category" value="2" class="custom-control-input">
					  <label class="custom-control-label" for="serial">Сериал</label>
					</div>
			      </div>
			    </div>
			  </fieldset>
			  <button type="submit" class="btn btn-primary float-right">Добавить</button>
			</form>
		</div>
	</div>
@endsection