<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\User;

class Tiding extends Model
{
    protected $fillable = ['title', 'body', 'img'];

    public function comments()
    {
    	return $this->morphMany(Comment::class, 'commentable');
    }

    public function addComment($text)
    {
    	Comment::create([
    		'text' => $text,
    		'commentable_id' => $this->id,
            'commentable_type' => get_class($this),
    		'user_id' => auth()->id()
    	]);
    }
}