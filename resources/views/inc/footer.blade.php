<footer class="footer mt-auto py-3 text-center">
  <div class="container">
  	<hr>
    <p><span style="color: black">Blog template built for</span> <a href="https://getbootstrap.com/">Bootstrap</a> <span style="color: black">by</span> <a href="https://twitter.com/mdo">@mdo</a>.</p>
  <p> <a href="#">Back to top</a> </p>
  </div>
</footer>