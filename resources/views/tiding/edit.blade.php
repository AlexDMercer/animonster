@extends('admin.app')

@section('title')
	Редактирование новости
@endsection

@section('content')
	<h2>Редактировать новость</h2>
	<div class="card">
		<div class="card-body">
			<form action="{{route('tidings.update', $tiding->id)}}" method="POST">
				{{ method_field('PUT') }}
				@csrf 
				<div class="form-group">
				    <label for="img">Постер</label>
				    <input type="text" name="img" class="form-control" id="img" placeholder="URL постера" value="{{$tiding->img}}">
			  	</div>
				<div class="form-group">
				    <label for="title">Заголовок</label>
				    <input type="text" name="title" class="form-control" id="title" value="{{$tiding->title}}">
				</div>
				<div class="form-group">
				    <label for="body">Текст новости</label>
				    <textarea type="text" name="body" class="form-control" id="body">{{$tiding->body}}</textarea>
				</div>
			  	<button type="submit" class="btn btn-dark float-right">Редактировать</button>
			</form>
		</div>
	</div>
@endsection