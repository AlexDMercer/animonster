@extends('app')

@section('title')
	Сериалы
@endsection

@section('content')
	<h2>Сериалы</h2><hr>
	<div class="container">
		<div class="row">
			@foreach($films as $film)
				<div class="films-block mb-5 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
					<a href="{{route('show', $film->id)}}"><img src="{{$film->poster}}" alt=""></a>
					<div class="text-center"><a href="{{route('show', $film->id)}}">{{$film->name}}</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<div class="float-right">{{$films->render()}}</div>
@endsection