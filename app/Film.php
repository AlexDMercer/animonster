<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\User;
use App\Note;

class Film extends Model
{

    protected $fillable = ['name', 'description', 'category', 'year', 'author', 'rating', 'poster', 'frame'];

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function addComment($text)
    {
    	Comment::create([
    		'text' => $text,
    		'commentable_id' => $this->id,
            'commentable_type' => get_class($this),
    		'user_id' => auth()->id()
    	]);

    	//Другой способ
    	//$this->comments()->create(compact('text'));
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function notes()
    {
        return $this->belongsToMany(User::class)->withPivot('season', 'series');
    }
}
