<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Http\Request;
use App\User;
use App\Film;
use App\Note;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::all();
        $user = User::find(auth()->user()->id);
        return view('notes.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::find(auth()->user()->id);
        $films = Film::all();
        return view('notes.create', compact('user', 'films'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Film $film)
    {   
        $user = auth()->user();
        $user->notes()->attach(
            $request->name,
            ['season' => $request->season,
            'series' => $request->series]
        );
        return redirect()->route('notes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($note)
    {
        $film = Film::find($note);
        $films = Film::all();
        $user = auth()->user();
        $note = Note::where('film_id', $note)->where('user_id', $user->id)->first();

        return view('notes.edit', compact('user', 'note', 'films', 'film'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note, Film $film)
    {
        $note = Note::find($note->id)->update([
            'film_id' => $request->name,
            'season' => $request->season,
            'series' => $request->series
        ]);

        return redirect()->route('notes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->user();
        $user->notes()->detach($id);
        return redirect()->route('notes.index');
    }
}
