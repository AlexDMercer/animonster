<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Film;
use App\Comment;
use App\User;
use App\Tiding;

class CommentController extends Controller
{
    //Comments for Films

    public function store(Film $film, CommentRequest $request)
    {
    	//$this->validate(request(), ['text' => 'required']);
        $film->addComment(request('text'));
    	return back()->with('success', 'Комментарий добавлен');
    }

    public function edit(Film $film, Comment $comment)
    {
        return view('edit', compact('film', 'comment'));
    }

    public function update(Film $film, Comment $comment, CommentRequest $request)
    {
        //$request->film()->comments()->find($id)->update(['text' => $request->text]);
        $comment->text = $request->input('text');
        $comment->save();
        return redirect()->route('show', $film->id)->with('success', 'Комментарий отредактирован');
    }

    public function delete($id)
    {
        $comment = Comment::find($id)->delete();
        return back()->with('success', 'Комментарий удален');
    }

    public function check(Film $film, Request $request)
    {
        $liked = $comment->liked()->find(auth()->id());
        return response()->json(['liked' => $liked]);
    }

    public function like(Film $film, Request $request)
    {
        $user = auth()->id();
        $comment = Comment::find($request->comment);
        if ($comment->liked()->find(auth()->id())) {
            $comment->like_comment = $comment->like_comment -1;
            $comment->liked()->detach($user);
            $comment->save();
            $likes = $comment->like_comment;
            $comment_id = $comment->id;
            return response()->json(['comment_id' => $comment_id, 'likes' => $likes]);
        } else {
            $comment->like_comment = $comment->like_comment +1;
            $comment->liked()->attach($user);
            $comment->save();
            $likes = $comment->like_comment;
            $comment_id = $comment->id;
            return response()->json(['comment_id' => $comment_id, 'likes' => $likes]);
        }
    }

    public function dislike(Film $film, Request $request)
    {
        $user = auth()->id();    
    }


    //Comments for Tidings

    public function t_store(Tiding $tiding)
    {
        $this->validate(request(), ['text' => 'required']);
        $tiding->addComment(request('text'));
        return back()->with('success', 'Комментарий добавлен');
    }

    public function t_edit(Tiding $tiding, Comment $comment)
    {
        return view('tiding.edit_comment', compact('tiding', 'comment'));
    }

    public function t_update(Tiding $tiding, Comment $comment, CommentRequest $request)
    {
        //$request->film()->comments()->find($id)->update(['text' => $request->text]);
        $comment->text = $request->input('text');
        $comment->save();
        return redirect()->route('tidings.show', $tiding->id)->with('success', 'Комментарий отредактирован');
    }

    public function t_delete($id)
    {
        $comment = Comment::find($id)->delete();
        return back()->with('success', 'Комментарий удален');
    }

    public function ajax(Request $request)
    {

        $data = $request->num;
        return $data;
    }
}
