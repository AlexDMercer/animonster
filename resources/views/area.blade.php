@extends('app')

@section('title')
	Фильмы
@endsection

@section('content')
	<h2>Личный кабинет</h2><hr>
	<div class="container">
  		<table class="table table-striped table-dark">
		  <thead>
		    <tr>
		      <th scope="col" class="text-center">Дата</th>
		      <th scope="col" class="text-center">Посетителей</th>
		    </tr>
		  </thead>
		  <tbody>
			    <tr>
			      <td class="text-center">1</td>
			      <td class="text-center">2</td>
			    </tr>
		  </tbody>
		</table>
	</div>	
@endsection