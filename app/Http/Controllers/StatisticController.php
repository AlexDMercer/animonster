<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Statistic;
use App\Tiding;
use App\Film;

use Illuminate\Http\Request;

class StatisticController extends Controller
{
    public function index()
    {
    	$statistics = Statistic::orderBy('id', 'desc')->paginate(10);
    	return view('admin.index', compact('statistics', 'films', 'tidings'));
    }

    public function films()
    {
    	$films = Film::orderBy('view_count', 'desc')->paginate(10);
    	return view('admin.films', compact('films'));
    }

    public function sortFilms()
    {
        $films = Film::withCount('comments')->orderBy('comments_count', 'desc')->paginate(10);
        return view('admin.sortfilms', compact('films'));
    }

    public function tidings()
    {
    	$tidings = Tiding::orderBy('view_count', 'desc')->paginate(10);
    	return view('admin.tidings', compact('tidings'));
    }

    public function sortTidings()
    {
        $tidings = Tiding::withCount('comments')->orderBy('comments_count', 'desc')->paginate(10);
        return view('admin.sorttidings', compact('tidings'));
    }


    public function update(Statistic $statistic)
    {
    	# code...
    }
}
