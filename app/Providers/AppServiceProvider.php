<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;
use App\Film;
use App\Tiding;
use App\Statistic;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        if(!isset($_COOKIE['visit'])) {
            $statistic = Statistic::where('created_at', date("Y-m-d"))->first();
            $statistic->update(['visit' => $statistic->visit +1]);
            setcookie('visit', 'yes', time()+60*60*24);
        }

        $films_rating = Film::orderBy('rating', 'desc')->take(5)->get();
        $last_tidings = Tiding::orderBy('created_at', 'desc')->take(5)->get();
        view()->share(compact('films_rating', 'last_tidings'));
    }
}
